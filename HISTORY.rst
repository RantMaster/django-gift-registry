Release History
---------------

0.5.4 (2021-09-29)
++++++++++++++++++

Switch to setup.cfg.


0.5.3 (2021-09-22)
++++++++++++++++++

Upgraded for Python 3 and Django 3.2.


0.4.3 (2015-08-04)
++++++++++++++++++

Initial version.
